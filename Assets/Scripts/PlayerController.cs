﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    private int count;
    public float speed;
    public float jumpSpeed;

    private Rigidbody rb;
    public Text countText;
    public Text winText;
    private int scene;
  

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        scene = SceneManager.GetActiveScene().buildIndex;
        count = 0;
        winText.text = "";
        SetCountText();
        
    }
   
    private void FixedUpdate()
    {
        if (Input.GetButtonDown("Jump") && rb.velocity.y == 0)//Unity RigidBody.Velocity api
            rb.velocity = new Vector3(0, jumpSpeed, 0);
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();

        }
       
    }
 
    private void SetCountText()
        
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 10 && scene == 1)
        {
            winText.text = "You win!!!";
        }
        if (count >= 5 && scene == 2)
        {
            winText.text = "You win!!!";
        }
    }
}

